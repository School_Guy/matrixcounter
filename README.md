# MatrixCounter

The Link to the GSAK-Download-Page: [GSAK-Forums](https://gsak.net/board/index.php?act=ST&f=37&t=31548).<br>The Link to the GSAK-Support-Forum: [GSAK-Forums](https://gsak.net/board/index.php?act=ST&f=7&t=31547).

This macro generates statistics about your finds. You can choose between German and English as an output language and German an English as an interface language.

Learn how you can contribute from the file `CONTRIBUTING.md`.